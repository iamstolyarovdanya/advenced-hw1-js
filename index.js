class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;

    }

    get Getname() {
        return this.name
    }
    set Setname(name1) {
        this.name = name1;
    }

    get Getage() {
        return this.age
    }
    set Setage(age1) {
        this.age = age1;
    }

    get Getsalary() {
        return this.salary
    }
    set Setsalary(salary1) {
        this.salary = salary1;
    }
}

const artur = new Employee(`Artur`, 21, 1200);
console.log(artur);
artur.Setname = `Arturio`
artur.Setage = 33
artur.Setsalary = 3333;
console.log(artur);

class Programemer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get Getsalary() {
        let show_salary = (this.salary * 3);
        return show_salary

    }
    get Getlang() {
        return this.lang
    }
}
const anna = new Programemer(`Anna `, 21, 1000, `js`);
const maxi = new Programemer(`Maxim `, 40, 9000, `java`);
const lena = new Programemer(`Elena`, 22, 700, `PHP`)

console.log(anna);
console.log(maxi);
console.log(lena);



/*
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
Наслидовання через прототипи ми використовуемо для того ,  щоб меньше навантакжувати сторінку.Та властивості батька можуть використовувати нащадки.



Для чого потрібно викликати super() у конструкторі класу-нащадка?
Super() потрібно писать для того , щоб мати можливість вікорисовувати власивості які вписуються у змінні у бвтька.


*/